<?php 
require('frog.php');
require('ape.php');

$sheep = new animal("shaun");

echo "Nama Hewan: ". $sheep->name . "<br>";
echo "Jumlah Kaki: ". $sheep->legs . "<br>";
echo "Cold Blooded: ". $sheep->cold_blooded . "<br><br>";

$kodok = new frog("buduk");

echo "Nama Hewan: ". $kodok->name . "<br>";
echo "Jumlah Kaki: ". $kodok->legs . "<br>";
echo "Cold Blooded: ". $kodok->cold_blooded . "<br>";
echo "Jump: ". $kodok->Jump() . "<br><br>";


$sungokong = new ape("kera sakti");

echo "Nama Hewan: ". $sungokong->name . "<br>";
echo "Jumlah Kaki: ". $sungokong->legs . "<br>";
echo "Cold Blooded: ". $sungokong->cold_blooded . "<br>";
echo "Yell: ". $sungokong->yell() . "<br>";

?>