@extends('layout.master')

@section('judul')
Detail Film {{ $film->judul }}
@endsection
@section('content')
    <h1>{{ $film->judul }}</h1>
    <p>Genre : <span class="badge badge-warning">{{ $film->genre->nama }}</span></p>
    <p>Tahun : {{ $film->tahun }}</p>
    <div class="d-flex">
        <img src="{{ asset('storage/' . $film->poster) }}" class="img-fluid mx-3 col-sm-6" alt="">
        <p style="text-align: justify">{{ $film->ringkasan }}</p>
    </div>
    <div class="mt-5">
        <h3>Peran Utama :</h3>
        <img src="{{ asset('storage/' . $film->cast->image) }}" style="width: 100px" class="mt-2">
        <h5 class="mt-3">{{ $film->cast->nama }}</h5>
    </div>
    <hr>
    <h3>Komentar</h3>
    <hr>
    <div>
        @foreach ($film->kritik as $item)
            <div class="media border p-2">
                <h5 class="mr-3 ctext-info">{{ $item->point }}&#11088;</h5>
                <div class="media-body">
                    <h5 class="mt-0">{{ $item->user->name }}</h5>
                    <p>{{ $item->content }}</p>
                </div>
            </div>
        @endforeach
    </div>
    <hr>
    <h3>Tambah Komentar</h3>
    <hr>
    @auth
        <form action="/kritik" method="POST"> 
            @csrf
            <input type="hidden" name="film_id" value="{{ $film->id }}">
            <div class="form-group">
                <select name="point" class="form-control" id="">
                    <option value="">-- Pilih Penilaian --</option>
                    <option value="1">&#11088;</option>
                    <option value="2">&#11088;&#11088;</option>
                    <option value="3">&#11088;&#11088;&#11088;</option>
                    <option value="4">&#11088;&#11088;&#11088;&#11088;</option>
                    <option value="5">&#11088;&#11088;&#11088;&#11088;&#11088;</option>
                </select>
            </div>
            @error('point')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="form-group">
                <textarea name="content" class="form-control" cols="30" rows="10" placeholder="Isi Pesan...."></textarea>
            </div>
            @error('content')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <hr>
            <input type="submit" class="btn btn-primary btn-sm" value="Kirim">
        </form>
    @endauth
        
@endsection