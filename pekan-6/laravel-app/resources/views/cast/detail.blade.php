@extends('layout.master')

@section('judul')
    Detail Kategori
@endsection
@section('content')
<h1>{{ $cast->nama }}</h1>
<p>Umur : {{ $cast->umur }} Tahun</p>
<div class="d-flex">
    <img src="{{ asset('storage/' . $cast->image) }}" class="img-fluid mx-3 col-sm-6" width="300px" alt="Rey Mbayang">
    <p style="text-align: justify">{{ $cast->bio }}</p>
</div>
<div class="mt-5">
    <h3>Film yang telah diperan :</h3>
    <div class="row">

        @foreach ($films as $film)
        <div class="col-lg-4 d-flex">
            <img src="{{ asset('storage/' . $film->poster) }}" style="width: 100px" class="card-img-top mt-2">
            <div class=" mx-3 ">
                <h5 class="mt-3">{{ $film->judul }}</h5> 
                <a href="/film/{{ $film->id }}" class="btn btn-primary col-lg mt-4">Detail Film</a>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection